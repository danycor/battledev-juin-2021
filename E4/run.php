<?php
$handle = fopen('input2.txt', 'r');
//$handle = STDIN;

function aligne($scraps, $start) {
    $new_start = array_slice($scraps, $start, count($scraps)-$start);
    $new_end = array_slice($scraps, 0, $start);
    return array_merge($new_start, $new_end);
}

function countScrap($scraps) {
    $c_scraps = ['A' => 0, 'B' => 0, 'C' => 0, 'D' => 0, 'E' => 0, 'F' => 0, 'G' => 0, 'H' => 0, 'I' => 0, 'J' => 0,
        'K' => 0, 'L' => 0, 'M' => 0, 'N' => 0, 'O' => 0, 'P' => 0, 'Q' => 0, 'R' => 0, 'S' => 0, 'T' => 0, 'U' => 0,
        'V' => 0, 'W' => 0, 'X' => 0, 'Y' => 0, 'Z' => 0];
    foreach ($scraps as $scrap) {
        $c_scraps[$scrap]++;
    }
    return $c_scraps;
}

$nb_scrap = intval(fgets($handle));
$line = fgets($handle);

$scraps = str_split(str_replace("\n", "", $line));

$nb_possible = 0;

$time = microtime(true);

error_log("Start $time");

for($j=0;$j<$nb_scrap;++$j) {
    error_log("loop $j ". (microtime(true)-$time));
    $time = microtime(true);

    $new_scrap = aligne($scraps, $j);
    error_log("Align $j ". (microtime(true)-$time));
    $time = microtime(true);

    $first_part = array_slice($new_scrap, 0, count($scraps)/2);
    $sec_part = array_slice($new_scrap, count($scraps)/2, count($scraps)/2);
    error_log("Slice $j ". (microtime(true)-$time));
    $time = microtime(true);

    $scrap_1 = countScrap($first_part);
    $scrap_2 = countScrap($sec_part);
    error_log("Count $j ". (microtime(true)-$time));
    $time = microtime(true);

    $is_possible = true;
    foreach ($scrap_1 as $key => $value) {
        if(!array_key_exists($key, $scrap_2) || $scrap_1[$key] !== $scrap_2[$key]) {
            $is_possible = false;
            break;
        }
    }
    error_log("Check $j ". (microtime(true)-$time));
    $time = microtime(true);

    if ($is_possible) {
        $nb_possible++;
    }
}


print $nb_possible;