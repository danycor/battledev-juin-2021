<?php
$handle = fopen('input4.txt', 'r');
//$handle = STDIN;


function countScrap($scraps) {
    $c_scraps = ['A' => 0, 'B' => 0, 'C' => 0, 'D' => 0, 'E' => 0, 'F' => 0, 'G' => 0, 'H' => 0, 'I' => 0, 'J' => 0,
        'K' => 0, 'L' => 0, 'M' => 0, 'N' => 0, 'O' => 0, 'P' => 0, 'Q' => 0, 'R' => 0, 'S' => 0, 'T' => 0, 'U' => 0,
        'V' => 0, 'W' => 0, 'X' => 0, 'Y' => 0, 'Z' => 0];
    foreach ($scraps as $scrap) {
        $c_scraps[$scrap]++;
    }
    return $c_scraps;
}

function calcDif($a1, $a2) {
    $new_a = [];
    foreach ($a1 as $key => $val) {
        $new_a[$key] = $a1[$key] - $a2[$key];
    }
    return $new_a;
}

function isNull($a) {
    foreach ($a as $val) {
        if ($val !== 0) {
            return false;
        }
    }
    return true;
}

$nb_scrap = intval(fgets($handle));
$line = fgets($handle);

$scraps = str_split(str_replace("\n", "", $line));

$nb_possible = 0;
error_log($line);

$first_part = array_slice($scraps, 0, count($scraps)/2);
$sec_part = array_slice($scraps, count($scraps)/2, count($scraps)/2);

$scrap_1 = countScrap($first_part);
$scrap_2 = countScrap($sec_part);

$dif = calcDif($scrap_1, $scrap_2);

for($j=0;$j<$nb_scrap;++$j) {
    if(isNull($dif)) {
        $nb_possible++;
    }

    $val = $scraps[($j <($nb_scrap/2)) ? $j+$nb_scrap/2 : $j-$nb_scrap/2];
    $val2 = $scraps[$j];
    $dif[$val]++;
    $dif[$val2]--;

}
if(isNull($dif)) {
    $nb_possible++;
}
print $nb_possible;