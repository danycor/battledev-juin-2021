<?php
$handle = fopen('input5.txt', 'r');
//$handle = STDIN;

$board = [];

function error_case($board) {
    error_log("--START--");
    foreach ($board as $line) {
        error_log(implode('',$line));
    }
    error_log("--END--\n");
}

function testColumn($bord, $id) {
    $fill_start = count($bord);
    // detect start
    for($j=0;$j<count($bord);++$j) {
        if ($bord[$j][$id] === '#') {
            $fill_start = $j-1;
            break;
        }
    }
    // fill with tetris
    for($j=$fill_start;$j>$fill_start-4;--$j) {
        if ($j>=0) {
            $bord[$j][$id] = '#';
        }
    }

    $line_filled = 0;
    foreach ($bord as $line) {
        $is_fill = true;
        foreach ($line as $cell) {
            if ($cell === '.') {
                $is_fill = false;
                break;
            }
        }
        if($is_fill) {
            $line_filled++;
        }
    }

    return $line_filled>=4;
}


while(FALSE !== ($line = fgets($handle))) {
    $line = str_replace("\n", "", $line);
    $board[] = str_split($line);
}
error_case($board);
for($j=0;$j<10;++$j) {
    if (testColumn($board, $j)) {
        print("BOOM ".($j+1));
        break;
    }
}

if($j>=10) {
    print("NOPE");
}
